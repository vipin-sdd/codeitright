<?php

use Illuminate\Database\Seeder;

class ChatterTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {

        // CREATE THE USER

        if (!\DB::table('users')->find(1)) {
            \DB::table('users')->insert([
                0 => [
                    'id'             => 1,
                    'name'           => 'smartdata',
                    'email'          => 'admin@smartdata.com',
                    'password'       => '$2y$10$9ED4Exe2raEeaeOzk.EW6uMBKn3Ib5Q.7kABWaf4QHagOgYHU8ca.',
                    'remember_token' => 'RvlORzs8dyG8IYqssJGcuOY2F0vnjBy2PnHHTX2MoV7Hh6udjJd6hcTox3un',
                    'created_at'     => '2019-12-29 15:13:02',
                    'updated_at'     => '2019-12-18 14:33:50',
                ],
            ]);
        }

        // CREATE THE CATEGORIES

        \DB::table('chatter_categories')->delete();

        \DB::table('chatter_categories')->insert([
            0 => [
                'id'         => 1,
                'parent_id'  => null,
                'order'      => 1,
                'name'       => 'Php',
                'color'      => '#3498DB',
                'slug'       => 'php',
                'created_at' => null,
                'updated_at' => null,
            ],
            1 => [
                'id'         => 2,
                'parent_id'  => null,
                'order'      => 2,
                'name'       => 'Mean',
                'color'      => '#2ECC71',
                'slug'       => 'mean',
                'created_at' => null,
                'updated_at' => null,
            ],
            2 => [
                'id'         => 3,
                'parent_id'  => null,
                'order'      => 3,
                'name'       => 'Microsoft',
                'color'      => '#9B59B6',
                'slug'       => 'microsoft',
                'created_at' => null,
                'updated_at' => null,
            ],
            3 => [
                'id'         => 4,
                'parent_id'  => null,
                'order'      => 4,
                'name'       => 'Angular',
                'color'      => '#E67E22',
                'slug'       => 'angular',
                'created_at' => null,
                'updated_at' => null,
            ],
            4 => [
                'id'         => 5,
                'parent_id'  => 1,
                'order'      => 1,
                'name'       => 'Laravel',
                'color'      => '#227ab5',
                'slug'       => 'laravel',
                'created_at' => null,
                'updated_at' => null,
            ],
            5 => [
                'id'         => 6,
                'parent_id'  => 2,
                'order'      => 1,
                'name'       => 'Nodejs',
                'color'      => '#195a86',
                'slug'       => 'nodejs',
                'created_at' => null,
                'updated_at' => null,
            ],
            6 => [
                'id'         => 7,
                'parent_id'  => 1,
                'order'      => 2,
                'name'       => 'Cakephp',
                'color'      => '#195a86',
                'slug'       => 'cakephp',
                'created_at' => null,
                'updated_at' => null,
            ],
            7 => [
                'id'         => 8,
                'parent_id'  => null,
                'order'      => 2,
                'name'       => 'Machine Learning',
                'color'      => '#227ab5',
                'slug'       => 'ml',
                'created_at' => null,
                'updated_at' => null,
            ],
            8 => [
                'id'         => 9,
                'parent_id'  => null,
                'order'      => 2,
                'name'       => 'React',
                'color'      => '#227ab5',
                'slug'       => 'rr',
                'created_at' => null,
                'updated_at' => null,
            ],
            9 => [
                'id'         => 10,
                'parent_id'  => null,
                'order'      => 2,
                'name'       => 'React Native',
                'color'      => '#227ab5',
                'slug'       => 'rr',
                'created_at' => null,
                'updated_at' => null,
            ],
        ]);

        // CREATE THE DISCUSSIONS

        \DB::table('chatter_discussion')->delete();

        \DB::table('chatter_discussion')->insert([
            0 => [
                'id'                  => 3,
                'chatter_category_id' => 1,
                'title'               => 'PHP Parse error:  syntax error line 3',
                'user_id'             => 1,
                'sticky'              => 0,
                'views'               => 0,
                'answered'            => 0,
                'created_at'          => '2016-08-18 14:27:56',
                'updated_at'          => '2016-08-18 14:27:56',
                'slug'                => 'hello-everyone-this-is-my-introduction',
                'color'               => '#239900',
            ],
            1 => [
                'id'                  => 6,
                'chatter_category_id' => 1,
                'title'               => 'PHP Fatal error:  Uncaught Error:',
                'user_id'             => 1,
                'sticky'              => 0,
                'views'               => 0,
                'answered'            => 0,
                'created_at'          => '2016-08-18 14:39:36',
                'updated_at'          => '2016-08-18 14:39:36',
                'slug'                => 'login-information-for-test',
                'color'               => '#1a1067',
            ],
            2 => [
                'id'                  => 7,
                'chatter_category_id' => 1,
                'title'               => 'PHP Warning:  include(gfg.php)',
                'user_id'             => 1,
                'sticky'              => 0,
                'views'               => 0,
                'answered'            => 0,
                'created_at'          => '2016-08-18 14:42:29',
                'updated_at'          => '2016-08-18 14:42:29',
                'slug'                => 'leaving-feedback',
                'color'               => '#8e1869',
            ],
            3 => [
                'id'                  => 8,
                'chatter_category_id' => 3,
                'title'               => 'NullReferenceException',
                'user_id'             => 1,
                'sticky'              => 0,
                'views'               => 0,
                'answered'            => 0,
                'created_at'          => '2016-08-18 14:46:38',
                'updated_at'          => '2016-08-18 14:46:38',
                'slug'                => 'just-a-random-post',
                'color'               => '',
            ],
            4 => [
                'id'                  => 9,
                'chatter_category_id' => 3,
                'title'               => 'DivideByZeroException',
                'user_id'             => 1,
                'sticky'              => 0,
                'views'               => 0,
                'answered'            => 0,
                'created_at'          => '2016-08-18 14:59:37',
                'updated_at'          => '2016-08-18 14:59:37',
                'slug'                => 'welcome-to-the-smartdata-tech-forum-portal',
                'color'               => '',
            ],
        ]);

        // CREATE THE POSTS

        \DB::table('chatter_post')->delete();

        \DB::table('chatter_post')->insert([
                    0 => [
                        'id'                    => 1,
                        'chatter_discussion_id' => 3,
                        'user_id'               => 1,
                        'body'                  => '<p>Machine learning solution</p>',
                        'created_at' => '2016-08-18 14:27:56',
                        'updated_at' => '2016-08-18 14:27:56',
                    ],
                    1 => [
                        'id'                    => 5,
                        'chatter_discussion_id' => 6,
                        'user_id'               => 1,
                        'body'                  => '<p>Be nodejs expert</p>',
                    'created_at' => '2016-08-18 14:39:36',
                    'updated_at' => '2016-08-18 14:39:36',
                ],
                2 => [
                    'id'                    => 6,
                    'chatter_discussion_id' => 7,
                    'user_id'               => 1,
                    'body'                  => '<p>Get PHP solution ready</p>',
                'created_at' => '2016-08-18 14:42:29',
                'updated_at' => '2016-08-18 14:42:29',
            ],
            3 => [
                'id'                    => 7,
                'chatter_discussion_id' => 8,
                'user_id'               => 1,
                'body'                  => '<p>Lets check yourseld in C#, ASP .net</p>',
                'created_at' => '2016-08-18 14:46:38',
                'updated_at' => '2016-08-18 14:46:38',
            ],
            4 => [
                'id'                    => 8,
                'chatter_discussion_id' => 8,
                'user_id'               => 1,
            'body'                      => '<p>Let me ask you.</p>',
            'created_at' => '2016-08-18 14:55:42',
            'updated_at' => '2016-08-18 15:45:13',
        ],
        5 => [
            'id'                    => 9,
            'chatter_discussion_id' => 9,
            'user_id'               => 1,
            'body'                  => '<p>Dont need to go other place, Everything is here.</p>',
            'created_at' => '2016-08-18 14:59:37',
            'updated_at' => '2016-08-18 14:59:37',
        ],
        6 => [
            'id'                    => 10,
            'chatter_discussion_id' => 9,
            'user_id'               => 1,
            'body'                  => '<p>Be like the troublshooter.</p>',
            'created_at' => '2016-08-18 15:01:25',
            'updated_at' => '2016-08-18 15:01:25',
        ],
        ]);
    }
}
