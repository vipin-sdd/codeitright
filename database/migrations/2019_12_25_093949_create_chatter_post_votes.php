<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatterPostVotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatter_post_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chatter_post_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->enum('type', ['up', 'down']);
            $table->tinyInteger('votes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('chatter_post_votes', function (Blueprint $table) {
            $table->foreign('chatter_post_id')->references('id')->on('chatter_post')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatter_post_votes');
    }
}
