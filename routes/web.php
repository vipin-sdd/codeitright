<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect()->to('/forums');
});

Auth::routes();

Route::post('/upvote', 'VotingController@upVote')->name('post.upvote');
Route::post('/downvote', 'VotingController@downVote')->name('post.downvote');

Route::get('/home', 'HomeController@index')->name('home');
