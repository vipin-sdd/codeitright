<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DevDojo\Chatter\Models\Post;

class ChatterPostVote extends Model
{
    use SoftDeletes;
    
    public $timestamps = true;
    protected $fillable = ['chatter_post_id', 'user_id', 'type', 'votes'];
    protected $dates = ['deleted_at'];

    public function chatterPost()
    {
        return $this->belongsTo(Models::className(Post::class), 'chatter_post_id');
    }

    public function user()
    {
        return $this->belongsTo(config('chatter.user.namespace'));
    }
}
