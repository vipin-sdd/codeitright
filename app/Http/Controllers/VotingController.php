<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DevDojo\Chatter\Models\Post;
use App\Models\ChatterPostVote;

class VotingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upVote(Request $request)
    {
        $user = auth()->user();
        $post = Post::where('id',$request->post_id)->firstOrFail();
        $postUser = $post->user;
        $checkExistingPost = ChatterPostVote::where('user_id', $user->id)->where('chatter_post_id', $post->id)->first();
        if($postUser->id != $user->id && is_null($checkExistingPost)) {
            $result = ChatterPostVote::Create([
                'user_id' => $user->id,
                'chatter_post_id' => $post->id,
                'votes' => 5, 
                'type' => 'up'
            ]);
            if($result) {
                return response()->json([
                    'status' => true
                ]); 
            }
            return response()->json([
                'status' => false
            ]); 
        } 
        return response()->json([
            'status' => false
        ]);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function downVote(Request $request)
    {
        $user = auth()->user();
        $post = Post::where('id',$request->post_id)->firstOrFail();
        $postUser = $post->user;
        $checkExistingPost = ChatterPostVote::where('user_id', $user->id)->where('chatter_post_id', $post->id)->first();
        if($postUser->id != $user->id && is_null($checkExistingPost)) {
            $result = ChatterPostVote::Create([
                'user_id' => $user->id,
                'chatter_post_id' => $post->id,
                'votes' => -1, 
                'type' => 'down'
            ]);
            if($result) {
                return response()->json([
                    'status' => true
                ]); 
            }
            return response()->json([
                'status' => false
            ]); 
        } 
        return response()->json([
            'status' => false
        ]);    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
