<?php
if (! function_exists('checkUserVote')) {
    function checkUserVote($user_id = null, $post_id = null) {
        $checkExistingPost = App\Models\ChatterPostVote::where('user_id', $user_id)->where('chatter_post_id', $post_id)->first();
        $status = $checkExistingPost ? 1 : 0;
        return $status;
    }
}

if (! function_exists('getReputation')) {
    function getReputation($user_id = null) {
        $totalVotes = 0;
        $discussions = DevDojo\Chatter\Models\Post::where('user_id', $user_id)->pluck('id');
        if(count($discussions)) {
            $totalVotes = App\Models\ChatterPostVote::whereIn('chatter_post_id', $discussions)->sum('votes');
        }

        return $totalVotes;
    }
}
if (! function_exists('topRatedUsers')) {
    function topRatedUsers() {
            $topRatedUsers = \DB::select("SELECT sum(`votes`) as `totalvotes`, `name` from chatter_post_votes INNER JOIN chatter_post on chatter_post.id = chatter_post_votes.`chatter_post_id` INNER JOIN users ON users.id = chatter_post.user_id GROUP BY  chatter_post.user_id ORDER BY totalvotes desc limit 5");
            return $topRatedUsers;
        }
    }
?>